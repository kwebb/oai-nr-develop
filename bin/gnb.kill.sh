#!/bin/bash

ENBEXE="nr-softmodem.Rel15"

# Kill off running function.
killall -q $ENBEXE
sleep 1

# Do some cleanup.
screen -wipe >/dev/null 2>&1

exit 0

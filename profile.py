#!/usr/bin/env python

"""


Use this profile to instantiate an experiment using Open Air Interface 5g nr 
(https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases) 
to realize an end-to-end SDR-based mobile network. This profile includes the following resources:

  * SDR NR-UE (d430 + USRP X3x0) running OAI ('rue1')
  * SDR gNB (d430 + USRP X3x0) running OAI ('gnb1')

Powder startup scripts automatically configure OAI for the specific allocated resources.

Instructions:

###Environment setup

Be sure to setup your SSH keys as outlined in the manual; it's better
to log in via a real SSH client to the nodes in your experiment.
Also, to get the "softscope" to work for the UE (-d option), you will
need to have X11 working.

Pointers to get ssh keys installed:

	https://help.github.com/articles/generating-ssh-keys

	https://www.powderwireless.net/ssh-keys.php

Pointers to get X11 working.

For macos:

	https://www.xquartz.org
	
For windows:

	http://www.straightrunning.com/XmingNotes/

or

	https://www.cygwin.com
	
or

	https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux

###Running OAI

The Open Air Interface source is located under /opt/oai on the gnb1
and rue1 nodes.  It is mounted as a clone of a remote blockstore
(dataset) maintained by Powder.  Feel free to change anything in
here, but be aware that your changes will not persist when your
experiment terminates.


**For gNB**

Log onto the `gnb1` node.

Find the network interface connected to the USRP by running `ifconfig -a`
and looking for the interface with IP address `192.168.30.1`.

Change the MTU for that interface:

	sudo ip link set mtu 9216 dev ifname # change ifname with the usrp interface
	
Then run the eNodeB software:	

    cd /opt/oai/develop-nr/cmake_targets/ran_build/build/
    
    sudo ./nr-softmodem -O /local/repository/etc/gnb.conf

**For the NR-UE**

Log onto the `rue1` node.

Find the network interface connected to the USRP by running `ifconfig -a`
and looking for the interface with IP address `192.168.30.1`.

Change the MTU for that interface:

	sudo ip link set mtu 9216 dev ifname # change ifname with the usrp interface
	
Then run the UE software:

    cd /opt/oai/develop-nr/cmake_targets/ran_build/build/
    
    sudo ./nr-uesoftmodem --numerology 1 -r 106 --phy-test -C 3510000000 --usrp-args "addr=192.168.30.2"

The above commands will run the UE software without the "softscope" GUI.
To run the UE software with the softscope GUI, you can run the following
script:

	sudo /local/repository/bin/nrue_scope.start.sh


For more detailed information:

  * [Here...](https://gitlab.flux.utah.edu/jczhu/OAI-NR/blob/master/README.md)

"""

#
# geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.urn as URN

#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_NR_DS = "urn:publicid:IDN+emulab.net:powderteam+ltdataset+OAI-NR"
    OAI_NR_IMG = "urn:publicid:IDN+emulab.net+image+PowderTeam:update-nr-image"
    OAI_CONF_SCRIPT = "/usr/bin/sudo /opt/oai/phantomnet/bin/config_oai.pl"
    PC_HWTYPE = "d430"
    SDR_HWTYPE = "sdr"
    TENGIG = 10 * 1000 * 1000

def connectOAI_DS(node):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai-nr")
    bs.dataset = GLOBALS.OAI_NR_DS
    bs.readonly = False
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
pc.defineParameter("REAL", "Use real RF hardware?",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Check box to use real hardware for RF. Uncheck for simulated operation.")

pc.defineParameter("ATTEN", "Use attenuator matrix (with real hardware)?",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Check box to use radio hardware via RF attenuator matrix. Uncheck for over-the-air.")


pc.defineParameter("RDSET", "Attach OAI source remote dataset",
                   portal.ParameterType.BOOLEAN, True,
                   longDescription="Check box to attach the OAI source code remote dataset to each node.")
pc.defineParameter("NTYPE", "Node type",
                   portal.ParameterType.STRING, "d430", ["d430","xl170"],
                   longDescription="Select the type of node to use.")
pc.defineParameter("FIXED_UE", "Bind to a specific SDR device for UE",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of an SDR device to allocate (e.g., \'pnbase1\').  Leave blank to let the mapping algorithm choose.")
pc.defineParameter("FIXED_GNB", "Bind to a specific SDR device for gNB",
                   portal.ParameterType.STRING, "",  advanced=True,
                   longDescription="Input the name of an SDR device to allocate (e.g., \'pnbase2\').  Leave blank to let the mapping algorithm choose.")

params = pc.bindParameters()

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()

# Add gNB PC.
gnb1 = request.RawPC("gnb1")
gnb1.hardware_type = params.NTYPE
#gnb1.Desire("site-meb", 1.0)
gnb1.disk_image = GLOBALS.OAI_NR_IMG
if params.RDSET:
    connectOAI_DS(gnb1)
gnb1.Blockstore("gnb1_lbs", "/mybs").size = "10GB"

# Add NR-UE PC.
rue1 = request.RawPC("rue1")
rue1.hardware_type = params.NTYPE
#rue1.Desire("site-meb", 1.0)
rue1.disk_image = GLOBALS.OAI_NR_IMG
if params.RDSET:
    connectOAI_DS(rue1)
rue1.Blockstore("rue1_lbs", "/mybs").size = "10GB"

if params.REAL:
    # Add USRP link for PC node and set address.
    gnb1_usrp_if = gnb1.addInterface( "gnb1_usrp_if" )
    gnb1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )
    # Add X300 gNB device.
    usrp_gnb = request.RawPC( "usrp-gnb")
    if params.FIXED_GNB:
        usrp_gnb.component_id = params.FIXED_GNB
    usrp_gnb.hardware_type = GLOBALS.SDR_HWTYPE
    usrp_gnb.setUseTypeDefaultImage()

    # Link between gNB X3x0 and PC host
    enblnk = request.Link( "gNB-lnk", members = [gnb1_usrp_if, usrp_gnb] )
    enblnk.bandwidth = GLOBALS.TENGIG
    enblnk.setJumboFrames()

    # Add USRP link for PC node and set address.
    rue1_usrp_if = rue1.addInterface( "rue1_usrp_if" )
    rue1_usrp_if.addAddress( rspec.IPv4Address( "192.168.30.1", "255.255.255.0" ) )
    
    # Add X300 NR-UE device.
    usrp_ue = request.RawPC( "usrp-ue" )
    if params.FIXED_UE:
        usrp_ue.component_id = params.FIXED_UE
    usrp_ue.hardware_type = GLOBALS.SDR_HWTYPE
    usrp_ue.setUseTypeDefaultImage()

    # Create UE SDR -> PC link
    uelnk = request.Link( "ue-lnk", members = [rue1_usrp_if, usrp_ue] )
    uelnk.bandwidth = GLOBALS.TENGIG
    uelnk.setJumboFrames()

    # Add attenuator RF link if requested.
    if params.ATTEN:
        gnb1_rf = usrp_gnb.addInterface("usrp_gnb_rf1")
        rue1_rf = usrp_ue.addInterface("usrp_ue_rf1")
        rflnk = request.RFLink("rfl1")
        rflnk.addInterface(gnb1_rf)
        rflnk.addInterface(rue1_rf)
    
else:
    # Create NR-UE to gNB PC Ethernet link for performing simulated runs
    ethlnk = request.Link( "ue-gnb", members = [gnb1, rue1] )
    ethlnk.bandwidth = GLOBALS.TENGIG
    ethlnk.setJumboFrames()
    
#
# Print and go!
#
pc.printRequestRSpec(request)
